[TOC]

**解决方案介绍**
===============
该解决方案基于OpenVPN，帮助企业之间或者个人与公司之间建立虚拟私人网络加密通道及数据安全传输隧道。有以下用途：

1，可与实现网通、电信机房快速、安全通信

2，可以作为公司的远程办公环境，可与PPTPD服务器作为互备

3，可以穿透一些顽固型的防火墙，直接访问其后的局域网环境中的资源

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-openvpn.html

**架构图**
---------------
![架构图](./document/rapid-deployment-of-openvpn.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1.创建两台Linux弹性云服务器部署在不同的可用区，分别用于搭建OpenVPN的服务端和客户端。

2.创建两个弹性公网IP，用于OpenVPN环境部署及提供访问公网和被公网访问能力。

3.创建安全组，可以保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。

**组织结构**
---------------

``` lua
rapid-deployment-of-openvpn
├── rapid-deployment-of-openvpn.tf.json -- 资源编排模板
├── userdata
    ├── openVPN_server.sh  -- OpenVPN服务端脚本配置文件
    ├── openVPN_client.sh  -- OpenVPN客户端脚本配置文件
```
**开始使用**
---------------
1.登录[弹性云服务器](https://auth.huaweicloud.com/authui/login.html?service=https%3A%2F%2Fconsole.huaweicloud.com%2Fecm%2F%3FagencyId%3D8f3a7568dba64651869aa83c1b53de79%26region%3Dcn-north-4%26locale%3Dzh-cn%26cloud_route_state%3D%2Fecs%2Fmanager%2FvmList#/login)控制平台，首先选择一台弹性云服务器，单击远程登录 ，或者使用其他的远程登录工具进入Linux弹性云服务器。

**图1** 登录云服务器控制平台
![登录云服务器控制平台](./document/readme-image-001.png)

**图2** 登录Linux弹性云服务器
![登录Linux弹性云服务器](./document/readme-image-002.png)

1. 在Linux弹性云服务中输入账号和密码后回车。

   **图3** 登录弹性云服务器
   ![登录弹性云服务器](./document/readme-image-003.png)

1. 在客户端查看日志输入cat /var/log/openvpn.log

   当返回如下信息，则表示已经成功连接VPN

   **图4** 返回信息
   ![返回信息](./document/readme-image-004.png)