#!/bin/bash
sudo apt-get install openssl libssl-dev -y
sudo apt-get install lzop
sudo apt-get install openvpn -y
sudo apt-get install easy-rsa -y
apt -y install expect
/usr/bin/expect <<EOF
spawn scp root@192.168.0.103:/etc/openvpn/client.ovpn /etc/openvpn/
expect "Are you sure you want to "
send "yes\r"
expect "password:"
send "$1\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn scp root@192.168.0.103:/etc/openvpn/keys/ca.crt /etc/openvpn/
expect "password:"
send "$1\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn scp root@192.168.0.103:/etc/openvpn/keys/ta.key /etc/openvpn/
expect "password:"
send "$1\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn scp root@192.168.0.103:/usr/share/easy-rsa/keys/client1.key /etc/openvpn/
expect "password:"
send "$1\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn scp root@192.168.0.103:/usr/share/easy-rsa/keys/client1.crt /etc/openvpn/
expect "password:"
send "$1\r"
expect eof
exit
EOF
openvpn --daemon --cd /etc/openvpn --config client.ovpn --log-append /var/log/openvpn.log &