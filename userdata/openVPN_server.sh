#!/bin/bash
apt-get install openssl libssl-dev -y
apt-get install lzop
apt-get install openvpn -y
apt-get install easy-rsa -y
sed -i "s/\"US\"/\"CN\"/g" /usr/share/easy-rsa/vars
sed -i "s/\"CA\"/\"Guangdong\"/g" /usr/share/easy-rsa/vars
sed -i "s/\"SanFrancisco\"/\"Qingdao\"/g" /usr/share/easy-rsa/vars
sed -i "s/\"Fort-Funston\"/\"MyOrganization\"/g" /usr/share/easy-rsa/vars
cd /usr/share/easy-rsa/
source /usr/share/easy-rsa/vars
./clean-all
apt -y install expect
/usr/bin/expect <<EOF
spawn ./build-ca
expect "Country Name"
send "\r"
expect "State or Province Name"
send "\r"
expect "Locality Name"
send "\r"
expect "Organization Name"
send "\r"
expect "Organizational Unit Name"
send "\r"
expect "Common Name"
send "\r"
expect "Name"
send "\r"
expect "Email Address"
send "\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn ./build-key-server server
expect "Country Name"
send "\r"
expect "State or Province Name"
send "\r"
expect "Locality Name"
send "\r"
expect "Organization Name"
send "\r"
expect "Organizational Unit Name"
send "\r"
expect "Common Name"
send "\r"
expect "Name"
send "\r"
expect "Email Address"
send "\r"
expect "A challenge password"
send "\r"
expect "An optional company name"
send "y\r"
expect "Sign the certificate?"
send "y\r"
expect "1 out of 1"
send "y\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn ./build-key client1
expect "Country Name"
send "\r"
expect "State or Province Name"
send "\r"
expect "Locality Name"
send "\r"
expect "Organization Name"
send "\r"
expect "Organizational Unit Name"
send "\r"
expect "Common Name"
send "\r"
expect "Name"
send "\r"
expect "Email Address"
send "\r"
expect "A challenge password"
send "\r"
expect "An optional company name"
send "y\r"
expect "Sign the certificate?"
send "y\r"
expect "1 out of 1"
send "y\r"
expect eof
exit
EOF
/usr/bin/expect <<EOF
spawn ./build-key client2
expect "Country Name"
send "\r"
expect "State or Province Name"
send "\r"
expect "Locality Name"
send "\r"
expect "Organization Name"
send "\r"
expect "Organizational Unit Name"
send "\r"
expect "Common Name"
send "\r"
expect "Name"
send "\r"
expect "Email Address"
send "\r"
expect "A challenge password"
send "\r"
expect "An optional company name"
send "y\r"
expect "Sign the certificate?"
send "y\r"
expect "1 out of 1"
send "y\r"
expect eof
exit
EOF
./build-dh
openvpn --genkey --secret keys/ta.key
mkdir /etc/openvpn/keys
cp /usr/share/easy-rsa/keys/{ca.crt,server.{crt,key},dh2048.pem,ta.key} /etc/openvpn/keys/
gzip -d /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz
cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf /etc/openvpn/
sed -i "s/;proto tcp/proto tcp/g" /etc/openvpn/server.conf
sed -i "s/proto udp/;proto udp/g" /etc/openvpn/server.conf
sed -i "s/;client-to-client/client-to-client/g" /etc/openvpn/server.conf
sed -i "s/server.crt/keys\/\server.crt/g" /etc/openvpn/server.conf
sed -i "s/server.key/keys\/\server.key/g" /etc/openvpn/server.conf
sed -i "s/dh2048.pem/keys\/\dh2048.pem/g" /etc/openvpn/server.conf
sed -i "78d" /etc/openvpn/server.conf
sed -i "78 i ca keys/ca.crt" /etc/openvpn/server.conf
sed -i '143 i push \"route 192.168.0.0 255.255.255.0"' /etc/openvpn/server.conf
sed -i "s/;client-to-client/client-to-client/g" /etc/openvpn/server.conf
sed -i "s/;duplicate-cn/duplicate-cn/g" /etc/openvpn/server.conf
sed -i "246 i tls-auth keys/ta.key 0" /etc/openvpn/server.conf
sed -i "s/;log-append/log-append/g" /etc/openvpn/server.conf
sed -i "s/verb 3/verb 5/g" /etc/openvpn/server.conf
sed -i '/net.ipv4.ip_forward/s/0/1/' /etc/sysctl.conf
sed -i '/net.ipv4.ip_forward/s/#//' /etc/sysctl.conf
sysctl -p
iptables -I INPUT -p tcp --dport 1194 -m comment --comment "openvpn" -j ACCEPT
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -j MASQUERADE
mkdir /etc/iptables
iptables-save > /etc/iptables/iptables.conf
ufw disable
systemctl start openvpn@server
systemctl enable openvpn@server
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/client.ovpn
sed -i "s/;proto tcp/proto tcp/g" /etc/openvpn/client.ovpn
sed -i "s/proto udp/;proto udp/g" /etc/openvpn/client.ovpn
sed -i "s/remote my-server-1 1194/remote $1 1194/g" /etc/openvpn/client.ovpn
sed -i "s/client.crt/client1.crt/g" /etc/openvpn/client.ovpn
sed -i "s/client.key/client1.key/g" /etc/openvpn/client.ovpn
sed -i "s/;tls-auth/tls-auth/g" /etc/openvpn/client.ovpn
sed -i "s/verb 3/verb 5/g" /etc/openvpn/client.ovpn